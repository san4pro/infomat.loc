<?php

require 'config.php';

$controller = $_GET['controller'] ?? 'news';
$action = $_GET['action'] ?? 'index';

$controllerName = ucfirst($controller) . 'Controller';
$controllerPath = 'controllers/' . $controllerName . '.php';

if (file_exists($controllerPath)) {
    require $controllerPath;
    $controller = new $controllerName();
    if (method_exists($controller, $action)) {
        $controller->$action();
    } else {
        http_response_code(404);
        exit('Page not found');
    }
} else {
    http_response_code(404);
    exit('Page not found');
}