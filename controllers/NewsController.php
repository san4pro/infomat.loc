<?php

class NewsController
{
    private $model;

    public function __construct()
    {
        $this->model = new NewsModel();
    }

    public function index()
    {
        $news = $this->model->getAll();
        require 'views/news/index.php';
    }

    public function view()
    {
        $id = $_GET['id'];
        $news = $this->model->getById($id);
        require 'app/views/news/view.php';
    }

    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $title = $_POST['title'];
            $content = $_POST['content'];
            $this->model->add($title, $content);
            header('Location: /news/index');
        } else {
            require 'views/news/add.php';
        }
    }

    public function edit()
    {
        $id = $_GET['id'];
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $title = $_POST['title'];
            $content = $_POST['content'];
            $this->model->update($id, $title, $content);
            header('Location: /news/view?id=' . $id);
        } else {
            $news = $this->model->getById($id);
            require 'views/news/edit.php';
        }
    }

    public function delete()
    {
        $id = $_GET['id'];
        $this->model->delete($id);
        header('Location: /news/index');
    }
}