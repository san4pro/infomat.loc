<?php

class NewsModel
{
    private $mysqli;

    public function __construct()
    {
        $this->mysqli = connect();
    }

    public function getAll()
    {
        $result = $this->mysqli->query('SELECT * FROM news');
        $news = [];
        while ($row = $result->fetch_assoc()) {
            $news[] = $row;
        }
        return $news;
    }

    public function getById($id)
    {
        $stmt = $this->mysqli->prepare('SELECT * FROM news WHERE id = ?');
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_assoc();
    }

    public function add($title, $content)
    {
        $stmt = $this->mysqli->prepare('INSERT INTO news (title, content) VALUES (?, ?)');
        $stmt->bind_param('ss', $title, $content);
        $stmt->execute();
        return $this->mysqli->insert_id;
    }

    public function update($id, $title, $content)
    {
        $stmt = $this->mysqli->prepare('UPDATE news SET title = ?, content = ? WHERE id = ?');
        $stmt->bind_param('ssi', $title, $content, $id);
        $stmt->execute();
    }

    public function delete($id)
    {
        $stmt = $this->mysqli->prepare('DELETE FROM news WHERE id = ?');
        $stmt->bind_param('i', $id);
        $stmt->execute();
    }
}