//путь до модулей
var urls = "D:\OSPanel\domains\node_modules\";

// корневая папка проекта он же url сайта
var proj = "infomat.loc";
var urls_proj = urls + proj;
//рабочий каталог с файлами
var fproj = "\";
var urls_proj_fproj = urls_proj + fproj;


//модули
var gulp = require(urls + 'gulp');

//плагин который автоматом подключает другие плагины
//var gp = require(urls + '/gulp-load-plugins')();

var sass = require(urls + 'gulp-sass');
var browserSync = require(urls + 'browser-sync').create();
var autoprefixer = require(urls + 'gulp-autoprefixer');
var cssnano = require(urls + 'gulp-cssnano');


// =================таск который компилирует sass, добавляет префиксы и сжимает
gulp.task('sass', function() {
  return gulp.src('.'+ fproj +'css\*.scss') //берем источник

    //компилируем сас в css. выставляем читабельный css и не умираем при ошибке
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade:true})) //автопрефиксер
    .pipe(cssnano())
    .pipe(gulp.dest('.'+ fproj+'css'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('js', function () {
    return gulp.src('.'+ fproj +'js\*.js')
        //.pipe(browserify())
        //.pipe(uglify())
        .pipe(gulp.dest('.'+ fproj +'js\min\'))
});

// ============================================= таск который запускает сервер
gulp.task('server', function(){
  browserSync.init({
      proxy: 'infomat.loc'
  });

  //автосинхрон php файлов
  gulp.watch('.' + fproj + '*.php').on('change', browserSync.reload);

  //автосинхрон js файлов
  gulp.watch('.'+ fproj +'js/*.js').on('change', browserSync.reload);
});


// ======= следящий таск, при изменении файлов запускает соответсвующие функции
gulp.task('watch', function(){
  gulp.watch('.'+ fproj +'css\*.scss', gulp.series('sass'));
});

// ============================================== стандартный таск по умолчанию
gulp.task('default', gulp.series(
  gulp.parallel('sass','watch','server')//таски которые запускаются парарельно
));
