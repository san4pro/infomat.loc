<?php foreach ($news as $item): ?>
    <h2><a href="/news/view?id=<?= $item['id'] ?>"><?= $item['title'] ?></a></h2>
    <p><?= $item['content'] ?></p>
    <p><a href="/news/edit?id=<?= $item['id'] ?>">Edit</a> | <a href="/news/delete?id=<?= $item['id'] ?>" onclick="return confirm('Are you sure?')">Delete</a></p>
<?php endforeach; ?>

<p><a href="/news/add">Add News</a></p>